---
layout: 2021/post
section: proposals
category: workshops
author: David Vaquero Santiago
title: DockerDrupalWorkflow, un entorno de desarrollo para Drupal
---

## Descripción

En este taller se pondrá en marcha un entorno de desarrollo para Drupal 9 basado en Docker y Docker Compose basándose en DockerDrupalWorkflow. Durante el taller se realizará una instalación de un entorno de Drupal 9 desde cero desde el repositorio oficial, se verá como publicar el código del proyecto en un repositorio Git, cómo incorporar a nuevas personas al proyecto una vez ya iniciado y cómo realizar un despliegue de la aplicación.

## Objetivos a cubrir en el taller

* Conocer la herramienta DockerDrupalWorkflow
* Saber instalar las dependencias del proyecto
* Saber desplegar un entorno de desarrollo de drupal basado en docker y docker compose
* Saber publicar el código del proyecto
* Saber incoporar a nuevas personas una vez que el proyecto ya está desplegado
* Desplegar un entorno de testing con el código del proyecto

-   Web del proyecto: <https://gitlab.com/pepesan/dockerdrupalworkflow>

## Público objetivo

* Desarrolladores web Drupal y web en general
* Personal de despliegue de entornos web y Drupal en particular
* Arquitectos de sistemas y de entornos en la nube (SRE)
* Jefes de proyecto de desarrollo web y de sistemas
* Cualquier persona interesada en el desarrollo y despliegue de soluciones web

## Ponente(s)

David Vaquero Santiago

### Contacto(s)

-   Nombre: David Vaquero Santiago
-   Email: <pepesan@gmail.com>
-   Web personal: <https://cursosdedesarrollo.com/>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@pepesan>
-   Twitter: <https://twitter.com/dvaquero>
-   Gitlab: <https://gitlab.com/pepesan>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/pepesan?tab=repositories>

## Prerrequisitos para los asistentes

Si los asistentes quieren realizar la instalación dentro del taller deberán disponer de los requisitos necesarios descritos en: <https://gitlab.com/pepesan/dockerdrupalworkflow/-/blob/master/README.md>


## Comentarios
