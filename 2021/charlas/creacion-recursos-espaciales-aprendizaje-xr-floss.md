---
layout: 2021/post
section: proposals
category: talks
author: Antonio Montes de Miguel
title: Creación de Recursos Espaciales de Aprendizaje XR con FLOSS
---

Charla sobre la creación de recursos espaciales de realidad ectendida con FLOSS, enfocado desde y hacia el Medio Ambiente Rural, con-ciencia eco-tecnológica.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Buenas, mi nombre es Antonio Montes, colaboro con el grupo AVFloss de Medialab-Prado y propongo una charla sobre el tema que estoy investigando en el doctorado de Bellas Artes cuyo título inicial de tesis fue: "CREA con SAL en el MAR".

**_Creación de_**<br />
**_Recursos_**<br />
**_Espaciales de_**<br />
**_Aprendizaje_**<br />

**_con_**<br />

**_Software_**<br />
**_Abierto y_**<br />
**_Libre_**<br />

**_en el_**<br />

**_Medio_**<br />
**_Ambiente_**<br />
**_Rural_**<br />

Los principales temas enlazados son:

- La importancia del espacio. Tanto el entorno físico de enseñanza/aprendizaje (centrándome en el natural/rural). Como de la relación de los contenidos con la distribución en el espacio (método de loci/mapas mentales) en este caso en el espacio XR 3D extendido.
- Construcción/compartición del conocimiento de modo colaborativo. Espacios virtuales de colaboración.
- Polilla o luciérnada. Es decir, ser esclava deslumbrada por la tecnología, estampándose "adictivamente" contra la pantalla de colores; o ser creadora consciente de los procesos y conocedora de la misma.
- Gestión del gasto/consumo energético de estos procesos.
- El uso, capacidades y bondades del FLOSS para éstos medios/fines.
- Creación y trazados del gesto en el espacio 3D y en entornos XR con FLOSS.
- El ejemplo de Blender + Spoke para crear entornos virtuales para Mozilla Hubs y su posible integración con plataformas libres como Moodle.
- Propuesta de colaboraciones.

-   Web del proyecto: <http://8d2.es/course/view.php?id=41>

## Público objetivo

- Creador@s 3D.
- Profesor@s.
- Programadores con ganas de colaborar en integración espacios virtuales + Moodle.
- Agentes en entornos rurales (escuelas, asociaciones,...)

## Ponente(s)

Antonio Montes de Miguel

### Contacto(s)

-   Nombre: Antonio Montes de Miguel
-   Email:
-   Web personal: <http://inventadero.com/>
-   Mastodon (u otras redes sociales libres): <https://qoto.org/@inventadero>
-   Twitter: <https://twitter.com/inventadero>
-   GitLab: <https://gitlab.com/inventadero>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/inventadero>

## Comentarios

El formato de la charla sería Jitsi y en paralelo un espacio Mozilla Hubs enlazado a un Moodle en los que poder intervenir y colaborar, similar a éste: <http://8d2.es/course/view.php?id=63>
