---
layout: 2021/post
section: proposals
category: talks
author: Alejandro Rodríguez Antolín
title: Laboratorio portable de creación y producción sonora el proyecto AudioFLOSS
---

AudioFLOSS es una recopilación de softwares pre-existentes y contrastados, específicamente diseñados para la edición, producción y composición musical, ejecutados desde un mismo entorno/lanzador. El objetivo, dotar de una infraestructura FLOSS (Free/Libre Open Source Software) y portable (sin instalación) a usuarios de este tipo de programas, facilitando la migración del software privativo a soluciones de software libre. En esta ponencia veremos, con precisión, cómo se desarrolla este proyecto

## Formato de la propuesta

Indicar uno de estos:

-   [x]  Charla corta (10 minutos)
-   [ ]  Charla (25 minutos)

## Descripción

Música y tecnología comparten un sendero común desde hace tiempo. Son diversos los proyectos, ideas y estudios académicos surgidos en los últimos años que ilustran la multitud de posibilidades emergentes, posibles gracias a la unión de estos dos mundos. Sin embargo, siguen siendo muchos los progresos por hacer, especialmente a la hora de valorar el impacto real de los distintos softwares o herramientas digitales, así como la ejecución efectiva de diversas propuestas teóricas. Como respuesta a esto, y apoyando la creación y producción musical por parte de todo tipo de usuarios, nace el proyecto AudioFLOSS (de la mano de Alejandro Rodríguez y Miguel Ángel Rodríguez).

AudioFLOSS (software) es una recopilación de programas informáticos, pre-existentes y contrastados, específicamente diseñados para la edición, producción y composición musical, ejecutados desde un mismo entorno/lanzador. A través de una sistemática selección y revisión del software y su plataforma, se desarrolla un entorno de ejecución común de dichos programas, dando forma a un “laboratorio musical digital”.

El objetivo, dotar de una infraestructura FLOSS (Free/Libre Open Source Software) y portable (sin instalación) a los usuarios de este tipo de programas, facilitando la migración del software privativo a soluciones de software libre. Se instaura así una democratización de las herramientas necesarias para el trabajo musical, suscitando múltiples reflexiones tales como las posibilidades para la composición sonora individual y colectiva, la gestión de recursos económicos (costes de licencias) y logísticos (refiriéndonos a un proceso de no-instalación, es decir, no invasor con nuestros dispositivos, o las posibilidades futuras para el desarrollo de software gracias al open-source), o las variadas aplicaciones del proyecto en el entorno educativo/académico, entre otras.

A través del proyecto AudioFLOSS, en la presente comunicación, se proyectará un nuevo enfoque para la creación y producción musical digital, conectando ámbitos musical e informático/tecnológico, dotando de accesibilidad y posibilidades a los usuarios, además de suscitar numerosas reflexiones.

Además del trabajo de recopilación y portabilización del software, hemos programado una ventana a modo de lanzador de estas aplicaciones. Está escrita en Free Pascal y el código fuente se descarga junto al resto de programas. En la versión 2.0 de AudioFLOSS se pueden encontrar recopilados los siguientes programas:

-   Ardour
-   Audacity
-   AutoDrum
-   Carla
-   DarkWave
-   Helio
-   Hydrogen
-   Jamulus
-   LMMS
-   Mixxx
-   MuseScore
-   PureData
-   SuperCollider
-   SonicPi
-   Sunvox
-   TuxGuitar
-   Zrythm
-   ZynAddSubFX

-   Web del proyecto: <https://audiofloss.melisa.gal/>

## Público objetivo

Todos aquellos que quieran trabajar o experimentar con el sonido o la música, al margen de su ámbito académico/profesional y su nivel de habilidad (desde iniciados a profesionales en el ámbito de las tecnologías sonoras).

## Ponente(s)

Grado en Historia y Ciencias de la Música y la Tecnología Musical (Universidad Autónoma de Madrid, UAM), Máster y Doctorado en Estudios Artísticos, Literarios y de la Cultura, especialidad en Música y Artes Escénicas (UAM; doctorado en curso, realizando prácticas docentes).

Interés en la música, tecnología y Humanidades Digitales, así como la creación interdisciplinar/multimedia en relación a la música electroacústica y el arte sonoro, y la creación musical informática y automática/algorítmica

### Contacto(s)

-   Nombre: Alejandro Rodríguez Antolín
-   Email: <alejandro.rodrigueza@uam.es>
-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter:
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://ftp.cixug.es/audiofloss/>

## Comentarios
