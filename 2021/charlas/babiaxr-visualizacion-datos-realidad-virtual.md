---
layout: 2021/post
section: proposals
category: talks
author: David Moreno Lumbreras, Andrea Villaverde Hernández y Almudena Vázquez de Miguel
title: BabiaXR Visualización de Datos en Realidad Virtual
---

BabiaXR es un set de de módulos front-end para el navegador. Estos módulos están basados en A-Frame y Three.js y permiten realizar queries, filtrar y representar datos. El objetivo de BabiaXR es conseguir que sea muy fácil crear distintos tipos de visualizaciones de datos (gráficos de barras, de burbujas, ciudades...) haciendo uso del potencial de WebXR y de la programación front-end habitual.

En esta ponencia hablaremos sobre el proyecto BabiaXR y mostraremos algunos ejemplos.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Existen muchas herramientas que permiten analizar datos de diferentes maneras, la mayoría se componen de una aplicación full-stack, pero las librerías de front-end conocidas limitan la visualización a las dos dimensiones, muy pocas tratan de visualizar datos en navegador integrando otras tecnologías como WebXR. Este es el objetivo de BabiaXR, ya que se trata de un set de módulos front-end para análisis de datos en VR (realidad virtual) y 3D.

Las visualizaciones de datos en BabiaXR están basadas en A-Frame y Three.js, ofreciendo una colección de componentes que permiten realizar queries, filtrar data y crear diferentes tipos de gráficos, todo ello desarrollado utilizando simplemente tecnologías front-end. Entre las visualizaciones, aparecen algunas comunes como gráficos de barras, gráficos circulares, gráficos de burbujas, etc., aunque se está yendo más allá y explorando nuevas formas de mostrar datos en 3D y VR.

Un ejemplo es la representación de proyectos de software utilizando la metáfora de la ciudad, en la que se muestra un sistema de software como una ciudad en la que cada edificio un fichero y cada barrio el árbol de directorios al que pertenece. En esta charla, hablaremos sobre el proyecto BabiaXR, mostrando distintos ejemplos del potencial de WebXR y A-Frame con diferentes visualizaciones.

La idea es mostrar lo fácil que puede ser desarrollar este tipo de módulos simplemente utilizando JavaScript y otras librerías front-end. Por último, utilizaremos datos conocidos relacionados con el COVID-19 y los mostraremos haciendo uso de BabiaXR.

-   Web del proyecto: <https://babiaxr.gitlab.io/> / <https://gitlab.com/babiaxr/aframe-babia-components>

## Público objetivo

Desarrolladores, profesores, investigadores y público general.

## Ponente(s)

Somos un equipo de desarrollo de software libre. Hemos hecho presentaciones en otras conferencias de desarrollo libre como FOSDEM. Los ponentes del equipo serán David Moreno Lumbreras, Andrea Villaverde Hernández y Almudena Vázquez de Miguel.

### Contacto(s)

-   Nombre: Almudena Vázquez de Miguel
-   Email:
-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter:
-   GitLab: <https://gitlab.com/almuvazmi>
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios
