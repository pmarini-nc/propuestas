---
layout: 2021/post
section: proposals
category: projects
title: Oslint
---

Análisis de repositorios de GitHub con consejos de buenas practicas para proyectos open-source.

-   Web del proyecto: <https://oslint.angrykoala.xyz/>

### Contacto(s)

-   Nombre: Andrés Ortiz
-   Email: <angrykoala@outlook.es>
-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/the_angry_koala>
-   GitLab: <https://gitlab.com/angrykoala>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/angrykoala>

## Comentarios
