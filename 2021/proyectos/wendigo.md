---
layout: 2021/post
section: proposals
category: projects
title: Wendigo
---

Wendigo es una herramienta de automatización de navegadores orientada a testing (end-to-end y front-end) construida encima de Puppeteer.

-   Web del proyecto: <https://github.com/angrykoala/wendigo>

### Contacto(s)

-   Nombre: Andrés Ortiz
-   Email: <angrykoala@outlook.es>
-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/the_angry_koala>
-   GitLab: <https://gitlab.com/angrykoala>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/angrykoala>

## Comentarios
