---
layout: 2021/post
section: proposals
category: papers
author: victorhck
title: Aprende Vim (de la manera más inteligente)
---

Esta guía trata de cerrar la brecha entre el "vimtutor" y la propia ayuda de Vim, destacando solo las funcionalidades más importante para aprender las partes más útiles de Vim en el menor tiempo posible.

## Descripción

Es una guía para aprender a utilizar el editor Vim, empezando por temas simples y genéricos y finalizando por temas más complejos y concretos.
Indicado para personas que quieren dar una oportunidad al editor Vim, o quienes conocen Vim y quieren profundizar un poco más en otros temas de Vim.

-   Web del proyecto: <https://victorhck.gitbook.io/aprende-vim/>

## Autor/a/es

victorhck: usuario de la distribución openSUSE de GNU/Linux y colaborador en muchos proyectos enfocados en la difusión de GNU/Linux y el software libre...

### Contacto(s)

-   Nombre: victorhck
-   Email: victorhck@mailbox.org
-   Web personal: <https://victorhckinthefreeworld.com>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@victorhck>
-   Twitter:
-   GitLab: <https://gitlab.com/victorhck>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/victorhck/Aprende-Vim>

## Comentarios
