---
layout: 2022/post
section: proposals
category: workshops
author: Eduar2
title: Efectos audiovisuales de espectáculos teatrales con soluciones libres
---

# Efectos audiovisuales de espectáculos teatrales con soluciones libres

> Los últimos años he tenido al oportunidad de participar en algunos atractivos proyectos teatrales y musicales, encargándome de la elaboración y control de medios audiovisuales y efectos especiales.<br><br>
En este taller contaré mi experiencia, basada en recursos libres y el sistema operativo GNU-Linux, y mostraré algunos de los elementos empleados en estos proyectos, que incluyen tanto software libre para el trabajo con audio y vídeo, como scripts de bash para automatización de labores y creación de paneles de control.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial

-   Descripción:

> En esta charla con demostraciones prácticas se contará la aventura de convertirse de la noche a la mañana en "especialista en efectos especiales" para espectáculos teatrales, los retos que ha supuesto y la forma en la que el ponente los ha abordado intentando ser fiel a recursos y licencias libres, y trasteando con muy diversas soluciones tecnológicas que ha ido conociendo por el camino.<br><br>
Entre las soluciones de las que se hablará están:
* Presentaciones: LibreOffice Impress
* Hojas de cálculo: LibreOffice Calc
* Bibliotecas de medios audiovisuales: Freesound
* Bibliotecas de imágenes: PXHere, Pexels, Wikimedia
* Reproducción de vídeo: VLC y OMXPlayer
* Edición de audio: Audacity
* Edición de imágenes (¡y vídeo-morphing!): GIMP
* Projection Mapping: MapMap
* Interfaz control remoto OSC: Comando sendosc
* Interfaz con controladores musicales: Comandos sendmidi y aseqdump
* Interfaces gráficos simples: YAD (Yet Another Dialog)
* Transformación de vídeos: Comandos ffmpeg y mencoder
* Control de iluminacion: QLC+
* Control remoto radiofrecuencia: Firmware Tasmota
* Interacción remota a través de APIs: Comandos wget y wscat
* Sistemas operativos: Raspberry OS Lite
* Gestión de vídeo autoarrancable: Scripts "Video_Looper"<br><br>
...y, por supuesto...<br><br>
* /bin/bash
* /usr/bin/screen<br><br>
La duración estimada de la ponencia es de 1 hora.

-   Público objetivo:

> Cualquier persona a la que le guste cacharrear con ordenadores u otro tipo de dispositivos para poner cosas en funcionamiento. Pero especialmente a quienes tengan interés en el mundo artístico, el software libre, la cultura libre y makers.

## Ponente:

-   Nombre: Eduar2

-   Bio:

> Informático y docente de profesión, y entusiasta de la música, los juegos de mesa y el “cacharreo” con lo que cae en sus manos. Linuxero y devoto de las iniciativas abiertas, que desde su época universitaria intenta promocionar siempre que le es posible.<br><br>
Imparte esporádicamente charlas formativas sobre recursos TIC para la educación (Moodle, Scratch, administración electrónica...) y en el encuentro "Linux y Tapas 2018" realizo una ponencia sobre el Projection Mapping, una de las tecnologías que se abordará en esta charla.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
