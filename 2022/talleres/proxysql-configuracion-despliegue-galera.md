---
layout: 2022/post
section: proposals
category: workshops
author: Javier Jaramago Fernández
title: ProxySQL&#58 Configuración y despliegue con Galera
---

# ProxySQL: Configuración y despliegue con Galera

> Taller para aprender a realizar un despliegue desde cero de ProxySQL contra un Cluster de Galera.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial

-   Descripción:

> El taller tendrá como objetivo una duración de al menos 1:30 horas, seguramente exprimamos el tiempo final hasta las 2 horas para dar más detalles, si es posible. El contenido del taller sería:<br><br>
1. Repaso rápido de conceptos básicos de ProxySQL:
  * ¿Qué es y que puede hacer?
  * Features más usadas e interesantes para despliegues generales.<br><br>
NOTA: El asistente puede refrescar la memoria y podríamos hacer este primer punto más ligero viendo la charla de 2020 que ofrecía una introducción al proyecto: <https://commons.wikimedia.org/wiki/File:EsLibre2020_19S.P2.02_-_Javier_Jaramago_Fern%C3%A1ndez_-_Introducci%C3%B3n_a_ProxySQL.webm><br><br>
2. Instalación del proyecto:
  * Imágenes de docker
  * Repositorio en instalación<br><br>
3. Creación de nuestro playground. Familiarizarnos con el fichero de configuración y consola de Administración.<br><br>
4. Cambios y probamos configuración relevante para despligues genéricos:
  * Revisión de variables globales principales.
  * Configuración de servidores MySQL.
  * Configuración de usuarios y testeo de conexiones con backend.
  * Creación y pruebas de 'Query Rules'.
  * Introducción a las tablas de estadísticas.<br><br>
5. Configuración para monitorización de Cluster de Galera. Mientros lo configuramos:
  * Conceptos básicos de monitorización con ProxySQL.
  * Observabilidad del comportamiento de ProxySQL.<br><br>
6. Pruebas contra nuestro cluster: (Si tenemos tiempo...)
  * Sysbench como prueba sencilla de carga
  * Monitorización usando prometheus y graphana.

-   Web del proyecto: <https://github.com/sysown/proxysql>

-   Público objetivo:

>
- Administradores de sistemas.
- Administradores de bases de datos.
- Devops.
- Desarrolladores de sistemas con curiosidad.

## Ponente:

-   Nombre: Javier Jaramago Fernández

-   Bio:

> Ingeniero de sistemas, contribuidor OpenSource, actualmente en ProxySQL. Interesado en programación de sistemas, bases de datos, sistemas críticos y distribuidos.

### Info personal:

-   Web personal: <https://github.com/JavierJF>
-   Twitter: <https://twitter.com/@JavJF>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/JavierJF>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
