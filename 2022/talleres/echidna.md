---
layout: 2022/post
section: proposals
category: workshops
author: Xabier Rosas &#38; Jorge Lobo
title: Echidna&#58 programando sistemas físicos en entornos gráficos
---

# Echidna: programando sistemas físicos en entornos gráficos

> En este taller exploraremos la conexión entre el mundo físico y el digital mediante Echidna.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial

-   Descripción:

> En este taller exploraremos la conexión entre el mundo físico y el digital mediante Echidna, usando entornos gráficos de programación a través de distintas propuestas entre las que cada pareja de participantes podrán elegir incluida "Machine learning".

-   Web del proyecto: <http://www.echidna.es>
-   Público objetivo:

> Cualquier persona interesada en la robótica. No se requiere ningún conocimiento previo, pero resultará más fácil si se tiene alguna experiencia con entornos gráficos de programación, como Scratch. Serán bienvenidas niñas y niños a partir de 8 años acompañados por un adulto. Se trabajará por parejas.<br><br>Los asistentes deben llevar al menos un ordenador por pareja con el [IDE de Arduino](https://www.arduino.cc/en/Main/Software) y [Snap4Arduino](http://snap4arduino.rocks/) instalados.<br><br>
ES NECESARIO REGISTRARSE PARA ASISTIR A ESTE TALLER POR CUESTIONES DE DISPONIBILIDAD DE RECURSOS: <a href="https://eventos.librelabgrx.cc/events/527281fa-e0ba-4f4e-a604-649e5b455d4c" target="_blank">https://eventos.librelabgrx.cc/events/527281fa-e0ba-4f4e-a604-649e5b455d4c</a>

## Ponente:

-   Nombre: Xabier Rosas, Jorge Lobo
-   Bio:

> Xabier Rosas: Co fundador de Echidna Educación, tío de Escornabot. 36 años de docencia en FP.<br><br>
Jorge Lobo: Maestro, Técnico Especialista en Electrónica Industrial y el de Técnico Superior en Animación Sociocultural. Participa en proyectos de hardware libre para el aprendizaje de la programación y la robótica educativa como Escornabot y Echidna Educación.

### Info personal:

-   Twitter: <https://twitter.com/@xdesig> / <https://twitter.com/lobo_tic>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/xdesig> / <https://github.com/lobotic/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
