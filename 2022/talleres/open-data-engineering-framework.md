---
layout: 2022/post
section: proposals
category: workshops
author: César García
title: Open Data Engineering Framework v0.1
---

# Open Data Engineering Framework v0.1

> En este taller, exploraremos Open Data Engineering Framework (ODEF), una metodología para contribuir a la mejora de los datos abiertos desde la ciudadanía utilizando herramientas libres.<br><br>
En los últimos años, estamos asistiendo a múltiples iniciativas particulares y/o privadas que tratan de exponer problemas en el acceso a los datos abiertos, la calidad o completitud de los mismos. En múltiples casos, estas iniciativas son de tipo individual y no se basan en la colaboración entre múltiples personas o grupos.<br><br>
ODEF parte desde un punto de vista complementario: tratando de generar procesos colaborativos, importando las mejores prácticas de la ingeniería de datos. Para ello se propone la generación de entornos que utilicen herramientas libres y que puedan ser fácilmente replicables,de modo que cualquier persona pueda contribuir a mejorar los datos abiertos que se están ofreciendo en la actualidad desde distintas fuentes. En el taller explicaremos cómo funciona de forma práctica.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Remoto

-   Descripción:

> ODEF parte desde un punto de vista complementario: tratando de generar procesos colaborativos, importando las mejores prácticas de la ingeniería de datos. Para ello se propone la generación de entornos que utilicen herramientas libres y que puedan ser fácilmente replicables,de modo que cualquier persona pueda contribuir a mejorar los datos abiertos que se están ofreciendo en la actualidad desde distintas fuentes. En el taller explicaremos cómo funciona de forma práctica.<br><br>
Es necesario que los participantes dispongan de su propio equipo para poder seguir las prácticas de forma activa. Todas las herramientas a emplear se podrán instalar durante el taller (o en caso de no disponer de una conexión muy buena, se enviará un enlace previo para que se puedan descargar de forma previa al congreso).<br><br>
Creo que la temática del taller puede resultar muy relevante en estos momentos, en los que se está deteriorando la confianza en las instituciones sin que se proponga ningún tipo de propuesta positiva/constructiva.

-   Público objetivo:

> Personas interesadas en el análisis de datos, ingeniería de datos y/o los datos abiertos. También puede resultar interesante a gente interesada en mejorar la calidad de los datos abiertos que se ofrecen desde las distintas administraciones estatales, autonómicas o municipales.

## Ponente:

-   Nombre: César García

-   Bio:

> Mi nombre es César García. Durante más de diez años he trabajado para la administración, antes de abandonar este rol para dedicarme a investigar cómo impactan las nuevas tecnologías en la sociedad.<br><br>
Durante el último año me he enfocado en la ciencia de datos y cómo se puede aplicar a conjuntos de datos abiertos. Para mi sorpresa, muchos de estos conjuntos de datos contienen errores tremendos, que los hacen inutilizables. ¿Qué valor tiene la generación de datos abiertos si este proceso no se realiza de forma rigurosa?<br><br>
Multiples iniciativas están abordando este problema desde una perspectiva individual, pero ¿cómo podría resolverse de forma colectiva? Esta pregunta me ha llevado a explorar los principios de la ingeniería de datos, para preparar una propuesta que me gustaría compartir con cualquier persona interesada en esta cuestión. De momento, no es más que un borrador, que me gustaría compartir por primera vez en esLibre, al ser un público que pienso que puede estar interesado en este tipo de iniciativas cívicas.

### Info personal:

-   Web personal: <https://elsatch.github.io>
-   Twitter: <https://twitter.com/elsatch>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/elsatch> / <https://gitlab.com/elsatch>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
