---
layout: 2022/post
section: proposals
category: workshops
author: inventadero
title: CreAvatar Personal
---

# CreAvatar Personal

> Pasos necesarios para personalizar un Avatar en 3D, a partir de fotografías o dibujos, con Software Libre.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial

-   Descripción:

> 2h- Taller guiado Presencial+OnLine<br><br>
CONTENIDOS:
* Introducción al Software Libre, especialmente edición 3Dcon Blender y a los Espacios Virtuales de Mozilla.
* Diseño como elemento principal de creación: Dibujo y fotografía en la elaboración de personajes.
* Modelado básico de Objetos: Modo Edición de Vértices/ Aristas/ Planos. Transformaciones básicas.
* Pintar Textura: Conceptos de Mapeado, Textura, Sombreadores (Shaders). Técnica de calco (Stencil).
* Exportación modelos 3D a formato de archivo glTF: Permite guardar animaciones, esqueleto, etc ...<br><br>
[Cada uno de los 3 pasos ( Modelado/ Pintura/ Importación) dispone de un ejercicio resuelto al final.]<br><br>
OBJETIVOS:<br><br>
* Practicar el dibujo como proyección.
* Reflexionar sobre la(s) identidad(es).
* Realizar proceso con Software Libre.
* Disfrutar; aprendiendo y enseñando.<br><br>
MATERIALES:<br><br>
* Preliminares: Papel y lápices colores / cámara fotos.
* Desarrollo: Ordenador(es) + ratón 3 botones. (yo llevo el mío)
* Verificación funcional Avatar: Conexión internet.<br><br>
REQUERIMIENTOS SALA:<br><br>
* Mesa para dibujar.
* Mesa ordenadores.
* Proyector(es)/Monitor grande (si es posible, para ver a escala 1:1 los avatares e interactuar con ellos....)<br><br>
Al finalizar, cada cual tendrá su avatar personalizado para subirlo a espacios virtuales de mozilla hubs (u otro tipo de avatar si es el gusto). Y para quien no le de tiempo, por lo que fuera, siempre tiene los recursos online, para hacerlo tranquilamente en otro momento.

-   Web del proyecto: <https://8d2.es/course/view.php?id=63> / <https://hubs.mozilla.com/onzCG3d>

-   Público objetivo:

> Todas las edades, quien quiera jugetear con la creación de avatares:<br><br>
* Pequeñ@s y profan@s; pueden hacerse un dibujo/foto, que "vestirá" como textura un avatar simple.
* Mayores/iniciad@s; además entrar al "Modo Edición" de la malla del avatar, etc...
* Tod@s; aportar ideas y conocimiento al respecto.

## Ponente:

-   Nombre: inventadero

-   Bio:

> Dibüjante Técnico y Natural. Estudiante eterno y profe de Secundaria, Escuela de Arte, Entornos Colaborativos, cursos adaptados presenciales y online.<br><br>
Actividades similares:<br><br>
Con el equipo de trabajo AVfloss de mediaLab realizamos este curso piloto de forma OnLine el curso pasado, también impartimos talleres de Creación  de espacios virtuales.<br><br>
El taller se publicará en breve en el repositorio del Recursos Educativos Abiertos del Intef de PROCOMÚN (Comunidad 3D): <https://www.medialab-matadero.es/actividades/comunes-xr-taller-de-mozilla-hubs>

### Info personal:

-   Web personal: <https://inventadero.com/>
-   Mastodon (u otras redes sociales libres): <https://qoto.org/@inventadero>
-   Twitter: <https://twitter.com/@inventadero>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/inventadero>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
