---
layout: 2022/post
section: proposals
category: devrooms
author: Wikimedia España
title: Proyectos Wikimedia
---

# Proyectos Wikimedia

## Detalles de la propuesta:

-   Descripción:

> Todas las personas pueden colaborar en los proyectos Wikimedia. Wikipedia, Commons, Wikidata… son herramientas clave en la difusión del conocimiento libre. Sin embargo, no siempre sabemos cómo acercarnos a ellas o de qué forma podemos colaborar.<br><br>
El propósito de esta sala es ofrecer y facilitar a la comunidad distintas opciones para participar, desde la documentación fotográfica en Wikimedia Commons hasta la organización de eventos de edición colaborativa en Wikipedia.

-   Formato:

>
* Primera sesión. Wiki Takes: documentando la España vaciada en Wikimedia Commons (45 min).<br><br>
Esta sesión servirá para exponer las iniciativas Wiki Takes, actividades de uno o varios días en los que un grupo de personas se juntan para documentar un lugar o una zona concreta con el objetivo de compartir las fotografías en Wikimedia Commons y que luego puedan ser utilizadas, por ejemplo, en Wikipedia. En nuestro caso concreto, como forma de documentar el medio rural, proyecto en el que Wikimedia España está inmerso desde 2015.<br><br>
* Segunda sesión. Guía web para organizar editatones: una herramienta para la autogestión de eventos de edición colaborativos (45 min).<br><br>
En esta sesión se presentará uno de los materiales en los que Wikimedia España ha trabajado en los últimos meses. Se trata de una guía web que explica, paso a paso y de forma sencilla, el proceso que conlleva la organización de una jornada de edición de Wikipedia, en la cual un grupo de personas se junta para editar contenidos, generalmente en colaboración con una entidad u organización y sobre una temática específica.<br><br>
* Sesión práctica: salida por Vigo para documentar una serie de elementos y ver, de forma práctica, la forma de subir las imágenes a Commons y usarlas en Wikipedia (2 h).<br><br>
En esta sesión saldremos a la calle para llevar a cabo un pequeño Wiki Takes por Vigo, con el objetivo de mostrar la dinámica de organización y trabajo que proponemos a la hora de llevar a cabo estas acciones. Se seleccionará un conjunto de elementos para fotografiar y se explicarán herramientas que nos pueden ayudar en la actividad así como la forma de compartir ese material en Wikimedia Commons.

-   Público objetivo:

> Personas que se dedican a la investigación académica, agentes e instituciones culturales, creadores, participantes en la comunidad del software libre, wikimedistas y todas las personas que tengan interés en los temas a tratar.

## Comunidad que propone la sala:

-   Nombre: Wikimedia España

-   Info:

> Wikimedia España es el capítulo reconocido por la Fundación Wikimedia para operar en España. Somos una asociación sin ánimo de lucro y reconocida como entidad de utilidad pública que promueve el conocimiento libre y los proyectos Wikimedia, siendo Wikipedia el más conocido. Nos constituimos en 2011 y llevamos todos estos años trabajando para mejorar la accesibilidad, el uso y la participación en Wikipedia y sus proyectos hermanos tanto a nivel social como institucional.<br><br>
La visión del movimiento Wikimedia es conseguir un mundo en el que todas las personas tengan acceso libre a la suma del conocimiento y puedan participar en su construcción colectiva. Seguimos las filosofías del conocimiento y la cultura libres, que defienden el derecho fundamental de acceso a estos, y todos nuestros proyectos operan sobre plataformas de software libre.<br><br>
Todo el mundo tiene conocimiento para compartir. Desde escribir o mejorar un artículo en Wikipedia, a documentar manifestaciones culturales y subir las imágenes a Wikimedia Commons. Personas individuales, colectivos u organizaciones: todas pueden formar parte del ecosistema Wikimedia.

-   Web: <https://www.wikimedia.es>
-   Twitter: <https://twitter.com/@wikimedia_es>

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso
