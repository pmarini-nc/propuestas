---
layout: 2022/post
section: proposals
category: misc
author: Pablo Gutiérrez Toral (coordinador)
title: Calendario Científico Escolar 2022&#58 un proyecto de cultura libre para promover vocaciones STEM
---

# Calendario Científico Escolar 2022: un proyecto de cultura libre para promover vocaciones STEM

> El Calendario Científico Escolar se publica anualmente desde 2020. Esta iniciativa recoge aniversarios científicos y tecnológicos diarios, así como actividades complementarias, con el fin de fomentar la cultura científica en los centros educativos y poner a disposición del alumnado modelos referentes diversos que promuevan las vocaciones STEM.<br><br>
El proyecto se lleva a cabo de forma colaborativa por un grupo de personas voluntarias de ámbitos muy diversos. Esto ha permitido ir adaptando las propuestas para intentar lograr un proyecto público de cultura libre, más participativo y accesible.<br><br>
Todos los materiales están disponibles de modo libre y gratuito y promovemos su distribución y reutilización. Los formatos disponibles han permitido, además, desarrollar una serie de funcionalidades para favorecer su difusión.

## Detalles de la propuesta:

-   Tipo de propuesta: Tablón de proyectos

-   Descripción:

> El Calendario Científico Escolar 2022 es una iniciativa que recoge un aniversario científico o tecnológico para cada día del año con el objetivo de fomentar la cultura científica en los centros educativos y poner a disposición del alumnado modelos referentes que promuevan las vocaciones STEM.<br><br>
El proyecto está coordinado desde el Instituto de Ganadería de Montaña (IGM), un centro mixto del CSIC y la Universidad de León y dispone de financiación de la Fundación Española de Ciencia y Tecnología (FECYT). Para su elaboración, contamos con la colaboración de numerosas entidades y personas voluntarias del mundo científico y educativo, quienes trabajan con nuestro equipo en el desarrollo del mismo y en la diversificación de los canales de comunicación.<br><br>
El calendario se acompaña de una guía didáctica con orientaciones para su aprovechamiento educativo transversal en las clases. Estas propuestas didácticas parten de los principios de inclusión, normalización y equidad y se acompañan de pautas generales de accesibilidad.<br><br>
El calendario, la guía y el resto de materiales generados están registrados bajo la licencia Creative Commons 4.0 BY y alojados en el repositorio institucional Digital.CSIC y la plataforma de desarrollo colaborativo GitLab, pudiendo descargarse a través en la página web del IGM (http://www.igm.ule-csic.es/calendario-cientifico). Además, para favorecer su uso en las aulas y alcanzar nuevos públicos, todo el material está disponible en varios idiomas (11 actualmente, tras ir creciendo de forma colaborativa).<br><br>
Actualmente también disponemos de cuentas de Twitter, Telegram y Mastodon que publican las efemérides diarias. De igual forma, el calendario está actualmente integrado en asistentes de voz como Alexa o GoogleHome. También contamos con ficheros iCal para ser importados en un calendario online, para cada idioma.<br><br>
En relación a la temática de este congreso, nuestro trabajo busca contribuir a la alfabetización científica de la población a través de un proyecto que aspira a ser abierto, colaborativo y cada vez más accesible. Para ello, hemos utilizado formatos que estamos intentando enriquecer año a año. Actualmente estamos trabajando para que nuestros archivos estén disponibles en el repositorio Wikimedia Commons.<br><br>
El calendario también dispone de una cuenta de Telegram que publica las efemérides diarias en diferentes idiomas: https://t.me/CalendarioCientifico<br><br>
Los ficheros para importar el calendario en tu propia agenda están disponibles en http://www.igm.ule-csic.es/calendario-cientifico

-   Web del proyecto: <http://www.igm.ule-csic.es/calendario-cientifico>

-   Público objetivo:

> Este proyecto se dirige a todos los públicos, aunque nuestro principal objetivo es llegar a personas cercanas al mundo de la docencia o aquellas puedan estar más distanciadas de la actividad investigadora, para fomentar su acceso e interés hacia la ciencia y la tecnología.

## Ponente:

-   Nombre: Pablo Gutiérrez Toral (coordinador)

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://botsin.space/@calendario_cientifico_escolar>
-   Twitter: <https://twitter.com/@CalCientifico>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://gitlab.com/calendario-cientifico-escolar/calendario-cientifico-escolar.gitlab.io >

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
