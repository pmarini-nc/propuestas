---
layout: 2022/post
section: proposals
category: talks
author: Jesús Amieiro Becerra
title: Investindo no software libre a longo prazo a iniciativa «Five for the Future»
---

# Investindo no software libre a longo prazo a iniciativa «Five for the Future»

> Desde o ano 2014 unhas 100 empresas están financiando o software libre WordPress e todo o seu ecosistema (wordpress.org), a través da iniciativa «Five for the Future». Nesta sesión presentarei este proxecto, que pode servir de guía a outros para financiarse dun xeito sostible.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

> Un dos principais problemas que teñen os proxectos publicados baixo licenzas libres, como é o caso de WordPress, é o do seu financiamento, xa que moitas veces os promotores non son capaces de crear un modelo de negocio sostible, e o proxecto acaba morrendo por inanición. Nesta sesión:
* Explicarei a problemática ao redor do financiamento do software libre, así como os principais métodos de financiamento.
* Presentarei a iniciativa «Five for the Future», que desde o ano 2014 permite que o proxecto libre WordPress goce dun éxito que non deixa de crecer co paso dos días.

-   Web del proyecto: <https://wordpress.org/five-for-the-future/>

-   Público objetivo:

> Todo tipo de usuarios de software libre, xa que é unha iniciativa transversal.

## Ponente:

-   Nombre: Jesús Amieiro Becerra

-   Bio:

> Son Jesús Amieiro, enxeñeiro de telecomunicación e artesán das TIC. Traballo como programador en Automattic, enfocado na área de internacionalización e tradución (<https://translate.wordpress.org/>). Participo ou participei en comunidades de PHP, WordPress e de software libre.<br><br>
Publico semanalmente >https://www.lasemanaphp.com>, unha colección semanal de noticias, tutoriais e información relevante sobre PHP e o seu ecosistema: Laravel, Symfony, WordPress, Drupal ...

### Info personal:

-   Web personal: <https://www.jesusamieiro.com/>
-   Twitter: <https://twitter.com/JesusAmieiro>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/amieiro>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
