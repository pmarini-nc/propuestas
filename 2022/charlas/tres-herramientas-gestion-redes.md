---
layout: 2022/post
section: proposals
category: talks
author: Fernando Fernandez Mancera
title: Tres herramientas de gestión de redes para gestionarlas a todas
---

# Tres herramientas de gestión de redes para gestionarlas a todas

> La administracion de redes es un area muy conocida por los administradores y desarrolladores de sistemas. Sin embargo, el numero de herramientas existentes es muy amplio y no siempre se usa la herramienta adecuada para la situacion. NetworkManager suele ser una de las herramientas usadas y no siempre se hace un uso optimo de la misma.<br><br>
En esta charla veremos las diferentes herramientas que rodean a NetworkManager y como el uso adecuado de estas en determinados entornos pueden ser muy beneficioso. Veremos nmcli, nmstate y Ansible Network System Role (ansible network-role).

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

> En esta charla hablaremos en profundidad de las diferentes herramientas de redes que rodean a NetworkManager. NetworkManager es el conjunto de herramientas de configuracion de red estandar de Linux. Es compatible con una amplia gama de configuraciones de red, desde el escritorio hasta los servidores y moviles.<br><br>
Sin embargo, no todo el mundo conoce la variedad de herramientas que existen y en ocasiones se hace un uso no optimo de ellas. Discutiremos los diferentes entornos existentes (escritorio, servidor, contenedores, clusters, VMs..) y veremos como cada herramienta es muy util en su contexto.

-   Web del proyecto: <https://www.networkmanager.dev/>

-   Público objetivo:

> Desarrolladores de sistemas, ingenieros/as de redes, administradores de sistemas/redes e incluso usuarios/as de escritorio de Linux!

## Ponente:

-   Nombre: Fernando Fernandez Mancera

-   Bio:

> Fernando (ffmancera) es un ingeniero software de Sevilla, España. Se dedica principalmente a la ingeniería de redes y sistemas. Es miembro del grupo de software libre SUGUS GNU/Linux y trabaja como ingeniero software en Red Hat en el equipo de Networking Services. Ha contribuido a múltiples proyectos de software libre como Tor, el subsistema Netfilter y el kernel de Linux. Además, es co-mantenedor de nmstate y contribuye activamente a NetworkManager, Ansible Linux Network System Role y Nispor.

### Info personal:

-   Web personal: <https://ffmancera.net>
-   Twitter: <https://twitter.com/ffmancera>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/ffmancera>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
