---
layout: 2022/post
section: proposals
category: talks
author: Miguel Ángel Fernández
title: La estrella de mi comunidad es un bot&#58 ¿Dónde están los humanos?
---

# La estrella de mi comunidad es un bot: ¿Dónde están los humanos?

> Si comparamos los principales actores del desarrollo de software de hace una década con los que tenemos ahora, vemos que una de las grandes diferencias es la inclusión del uso de cuentas automáticas (bots) que facilitan tareas de desarrollo, mantenimiento y gestión. De seguir esta tendencia, en un futuro no lejano la actividad automática será cada vez más y más relevante en el devenir de los proyectos. A la hora de analizar las comunidades de desarrollo de software libre, es imprescindible ser capaz de identificar estas cuentas para poder estudiar la salud de la comunidad. Esta charla muestra algunos de los retos identificando bots y algunas soluciones en las que estamos trabajando.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

> El uso de cuentas automáticas (Bots) en proyectos de software libre ha ido en aumento a lo largo del tiempo. Estas cuentas facilitan ciertas tareas de mantenimiento y gestión, pero también complican el análisis de la actividad que la comunidad lleva a cabo dentro de dichos proyectos. A esto se le suma la dificultad identificar individuos (tanto humanos como bots) que usen cuentas diferentes en diversas herramientas comúnmente usadas en el desarrollo de proyectos de software.<br><br>
Diferenciar dichas cuentas automáticas y sus contribuciones de aquellas provenientes de desarrolladores humanos provoca un impacto cada vez mayor a la hora de analizar la información relativa a la actividad real generada por la comunidad en dichos proyectos.<br><br>
En esta charla se presentan los retos comunes a la hora de identificar cuentas automáticas en proyectos de software y soluciones para paliar este problema (como por ejemplo la creación de identidades únicas que agrupen diferentes perfiles procedentes de la misma o distintas fuentes de información). Además estudiaremos el impacto y la evolución de dichas cuentas en un subconjunto de repositorios de la Fundación Wikimedia durante los últimos 10 años, aproximadamente. De este estudio se plantea la posibilidad de observar las diferencias entre la actividad de humanos y bots, ver cómo evoluciona en el futuro y comparar con la actividad de otras comunidades.  

-   Web del proyecto: <https://chaoss.github.io/grimoirelab/>

-   Público objetivo:

> Cualquier persona que esté interesada en aprender más sobre las comunidades de software libre (desarrolladores o no), gestores de proyecto, community managers, analistas de datos, entre otros.

## Ponente:

-   Nombre: Miguel Ángel Fernández

-   Bio:

> Soy consultor y analista de datos en Bitergia. También participo en GrimoireLab, parte del proyecto CHAOSS y en el departamento de LibreSoft de la Universidad Rey Juan Carlos.<br><br>
Soy uno de los contribuidores de SortingHat, una herramienta libre para la gestión de identidades y he dado otras charlas en eventos como FOSDEM, CHAOSSCon y OpenSouth Code.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
