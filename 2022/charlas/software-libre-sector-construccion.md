---
layout: 2022/post
section: proposals
category: talks
author: Evelio Sánchez Juncal
title: Software libre en el sector de la construcción
---

# Software libre en el sector de la construcción

> Año 2022 y seguimos construyendo, esencialmente, como lo hacían los romanos. El sector AECO es de los menos productivos a pesar de ser de los que más peso tienen en los PIB de casi cualquier país. El nivel de digitalización del sector es bajo,  muy bajo, y la ausencia de herramientas de código abierto no hace nada por mejorarlo.<br><br>
A pesar de lo anterior, existen iniciativas de código abierto específicas para el sector, no siempre de conocimiento general, y un cierto desconocimiento por parte del sector de las herramientas de código abierto que podrían ser utilizadas.<br><br>
También existe un cierto desconocimiento de las necesidades del sector por parte de los desarrolladores, más centrados en otros sectores, que lleva a la pérdida de oportunidades de desarrollar para un sector con una gran demanda no cubierta.<br><br>
Esta charla trata de presentar un escenario donde hay mucho hecho que se desconoce y donde queda mucho por hacer.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

> Presentación en formato tradicional: ponente con apoyo gráfico, con la que fomentar el debate y la reflexión por parte de los asistentes.

-   Público objetivo:

> Desarrolladores interesados en explorar un nicho de mercado poco explotado y con carencias importantes de aplicaciones y herramientas abiertas.

## Ponente:

-   Nombre: Evelio Sánchez Juncal

-   Bio:

> Arquitecto y Máster en Rehabilitación y Renovación Urbana. En paralelo con su actividad edificatoria ha desarrollado gran parte de su carrera en los aspectos más relacionados con la parte económica del mundo de la construcción, con el foco centrado en la valoración de inmuebles tanto residenciales como terciarios y en el análisis de viabilidad de proyectos inmobiliarios.<br><br>
Su pasión por la tecnología le ha valido para contactar y evaluar los más distintos aspectos del software relacionados directa o indirectamente con el mundo BIM, adentrándose en la automatización de procesos y la programación aplicada a la optimización de flujos de trabajo. Apasionado defensor de los formatos abiertos y el software open source. En contacto con la tecnología BIM desde 1.999

### Info personal:

-   Twitter: <https://twitter.com/evelio_sj>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
