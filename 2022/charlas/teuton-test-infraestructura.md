---
layout: 2022/post
section: proposals
category: talks
author: David Vargas Ruiz
title: TEUTON&#58 Test de infraestructura
---

# TEUTON: Test de infraestructura

> Teuton chequea tu infraestructura como si fuera código. Si te gusta TDD (Desarrollo dirigido por tests) de la programación... entonces si tienes un perfil teacher/student/syadmin/devops/sre te va a gustar Teuton.  Teuton lleva la filosofía TDD a tus dispositivos físicos: PCs, laptops, routers, switchs, smartphones, etc.<br><br>
Además es una gran herramienta para que el profesor sysadmin NO TENGA QUE CORREGIR. Ya sólo por eso... ¡merece la pena! ¿No crees?

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Remoto
-   Descripción:

> La charla es para divulgar un proyecto de software libre desarrollado en Canarias. TeutonSoftware es una plataforma que aglutina a varios proyectos, pero en esta charla vamos a hablar de Teuton.<br><br>
Teuton es test de infraestructura, libre, multiplataforma y muy sencillo de usar. Un sysadmin tarda 5 minutos en empezar a usarlo. Nosotros (profesores de FP informática) llevamos años usándolo para corregir las actividades realizadas por nuestros alumnos en sus máquinas reales o vituales, locales o remotas, LAN o WAN. El límite es SSH o Telnet.

-   Web del proyecto: <https://rubygems.org/gems/teuton>

-   Público objetivo:

> Sysadmin, devops, SRE, Profesores de informática, alumnos de informática y jueces de concursos de informática.

## Ponente:

-   Nombre: David Vargas Ruiz

-   Bio:

> Ingeniero Informático por la ULPGC. Profesor FP de Sistemas y Aplicaciones en el IES Puerto de la Cruz. IloveRuby, SoftwareLIbre, OpenSUSE y StarWars. Ha participado en Chapi, TLP, eComputing, FLISOL y PyDay.

### Info personal:

-   Web personal: <https://github.com/dvarrui>
-   Twitter: <https://twitter.com/@dvarrui>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/dvarrui>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
