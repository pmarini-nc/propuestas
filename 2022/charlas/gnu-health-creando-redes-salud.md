---
layout: 2022/post
section: proposals
category: talks
author: Luis Falcon
title: La Federación GNU Health&#58 Creando redes de salud e investigación con software libre
---

# La Federación GNU Health: Creando redes de salud e investigación con software libre

> En esta charla nos centraremos en la integración de los distintos componentes de GNU Health, el ecosistema de salud digital libre, para formar redes de salud e investigación en biomedicina. Presentaremos la próxima versión de MyGNUHealth, el registro personal de salud y cómo se integra en la Federación.<br><br>
Mostraremos la adopción de GNU Health en distintos proyectos de Medicina Social y Salud Pública, investigación sobre el cáncer y enfermedades neurodegenerativas, entre otros.<br><br>
Finalmente hablaremos de la Alianza GNU Health de Instituciones Académicas y de Investigación y cómo ponerlo en práctica en España.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

> GNU Health es el ecosistema de salud digital libre, adoptado por gobiernos, instituciones de salud y profesionales alrededor del mundo. Hace pocas semanas, GNU Health ha sido declarado bien público digital.<br><br>
Presentaremos los principales componentes y cómo integrarlos en la federación GNU Health para crear redes de salud y de investigación biomédica.<br><br>
Mostraremos ejemplos específicos de aplicación de GNU Health en Salud Pública, Medicina Social e investigación sobre el cáncer y enfermedades neurodegenerativas.<br><br>
Dedicaremos un espacio a MyGNUHealth 2.0, la próxima versión el gestor personal de salud (PHR), para escritorios y dispositivos móviles. MyGNUHealth registra parámetros de las distintas esferas de la salud (bio-psico-social) del individuo, con especial énfasis en la privacidad. En el contexto de la Federación de GNU Health, MyGNUHealth permite compartir con su profesional de salud en tiempo los resultados que la persona considere oportuno.<br><br>
Finalmente hablaremos de casos de implementación en distintos escenarios, y los beneficios que la federación de GNU Health puede aportar al sistema de salud pública, académico y de investigación.

-   Web del proyecto: <https://www.gnuhealth.org>

-   Público objetivo:

>
* Público general
* Profesionales de la salud
* Administración pública
* Investigadores en Ciencias Sociales y Biomedicina
* Instituciones académicas

## Ponente:

-   Nombre: Luis Falcon

-   Bio:

> Luis Falcón es un médico, científico computacional y activista español. Obtuvo el título en Ciencias de la Computación y Matemáticas en California State University, Northridge (USA) y de Medicina en el IUCS, Buenos Aires (Argentina). Posee un Masters en Genómica y Genética Médica por la Universidad de Granada (España). Es un activista social y de los derechos de los animales. Fundador y presidente de GNU Solidario, ONG focalizada en Medicina Social. Es el autor de GNU Health.

### Info personal:

-   Web personal: <https://www.meanmicio.org>
-   Mastodon (u otras redes sociales libres): <https://todon.eu/@meanmicio>
-   Twitter: <https://twitter.com/meanmicio>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
