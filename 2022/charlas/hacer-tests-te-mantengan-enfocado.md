---
layout: 2022/post
section: proposals
category: talks
author: Filis Futsarov
title: Cómo hacer que tus tests te mantengan enfocado
---

# Cómo hacer que tus tests te mantengan enfocado

> Al comenzar la charla, destacaré la importancia de tener una _test suite_ rápida apoyándome en lo que nos dice la psicología al respecto, especialmente en cómo afectan a nuestro cerebro las distintas escalas de tiempo para obtener el resultado de algo que hemos hecho (_feedback_), siendo un _feedback-loop_ corto el adecuado para seguir en el _mood_ y un _feedback-loop_ largo contraproducente para nuestro enfoque.<br><br>
La charla se centrará en qué malas prácticas debemos evitar, y posteriormente en cómo obtener una _test suite_ sana, haciendo especial hincapié en los tests de aceptación. Para ello, introduciré la arquitectura "Puertos y Adaptadores" y enseñaré cómo aprovecharla para sacarle el máximo partido para nuestra _test suite_.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

>Todos lo hemos visto, parece que a la hora de hablar de diseño de software todo se centra en la parte de código de producción. Nombres de métodos que revelan intención, clases cohesivas, poco acoplamiento... pero poco se habla de la aplicación de buenas prácticas y principios en nuestra otra "gran parte del proyecto": nuestra _test suite_.<br><br>
Parece que por alguna razón, cuando de hablar de tests se trata, relajamos todas esas buenas prácticas, principios y búsqueda continua de que todo funcione de forma óptima, algo que con el paso del tiempo, conduce hacía tests de baja calidad (_slow feedback-loop_, _flakiness_ y _fragility_).<br><br>
A diferencia de código de producción, ningún _Product Owner_ se quejará de que los tests van lentos, y tampoco saltará una alerta en slack para indicarnos de que nuestros tests van más lentos de lo debido.<br><br>
Es a los propios desarrolladores a quienes más influye una _test suite_ en mal estado, siendo la causa principal un _feedback-loop_ lento, que hace que nuestro enfoque sobre lo importante empeore, afectando así en gran medida a nuestra _overall Developer Experience_ por ser algo tan importante de nuestro día a día.<br><br>
Por todo esto podemos decir que tener una _test suite_ sana es de vital importancia para cualquier proyecto, por ello, en esta charla quiero mostrar:
* Importancia de tener una _test suite_ rápida desde el punto de vista de la psicología
* Arquitectura Puertos y Adaptadores y sus beneficios _out of the box_ para testing
* Diferencias entre Test de Aceptación integrado y Test de Aceptación desacoplado
* _Contract Testing / Adapter Tests_ y cómo pueden ayudarnos para nuestros Tests de aceptación
* Herramienta de análisis estático de código para validar nuestra arquitectura, en caso de que de tiempo (php architecture tester / phpat)<br><br>
¡Incluiré ejemplos de código también!

-   Público objetivo:

> Cualquier interesado en mantener una test suite sana. Developers, QAs, DevOps

## Ponente:

-   Nombre: Filis Futsarov

-   Bio:

> Passionate software developer for building high-quality and human-centred tech. Very interested in testing as a living documentation tool for developers.

### Info personal:

-   Web personal: <https://filis.me>
-   Twitter: <https://twitter.com/filisdev>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/filisko>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
