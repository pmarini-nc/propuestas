---
layout: 2022/post
section: proposals
category: talks
author: FurmanS
title: Bootstrap the world for your tests
---

# Bootstrap the world for your tests

> Services typically interact with other services. The communication and interactions between then is achived by different communications protocols, ports etc. This kind of soutions brings complexity in regard to execution.<br><br>
In order to execute a User Story that enclose different services,, we need to find a way to run all required pices, chunks wihtin their dependencies, configurations or run-up modes. Once we simulate such an environment or the majority of it, we are ready to execute the tests.<br><br>
One of the main aims is to find a declarative way to define a scope of the system, and assemble it. Once this is complete, we can programmatically configure, run and perform the test.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

> During this talk we are going to see how an open source library that could help us to spin up the whole "world". The main aim is to have a way boostrap the System Under Test (SUT) in a state that could be considered as a valid - what we are looking for is to have a SUT in a consistent up, running and ready to performance Integration Test or even end-to-end test throught it.<br><br>
Assembling, and connection all dots togheter is one for the big effort for DEV, QA and DEV-OPS teams. However this is one of the core part for a fast loopback inside the category of any kind of computer application craftsmakers.<br><br>
We are going break down this challenging topic and see one of the possible solution for the system made in microservices archtecture flavour.<br><br>
What we need - is a thin layer that will permit us in declarative way moreover in a programmatically way . In this case we are gaing freedom speaking about whatever whatever test that have to be cover within the System Under Test.<br><br>
Of coruse without Docker this could not be possible - but this time we need a little more power on top of it. To cover that, we need a functional programming language, particularly Scala with extra boost gained by cats lib.<br><br>
Aunque puso todo en Inglés, la charla, por favor, quiero darla en Castellano. A ver si le hacemos este año junto codo con codo.

-   Web del proyecto: <https://www.testcontainers.org>

-   Público objetivo:

> Developers, QA, Dev-Ops.

## Ponente:

-   Nombre: FurmanS

-   Bio:

> Enthusiast of the functional paradigm, computers, and human languages.
Traveler, musician. A Guitar artist of SolYNaranjaS - the multinational music project from Málaga city, Costa del Sol. Co-organiser of Málaga Scala Developers community during 2019-2021.

### Info personal:

-   Web personal: <https://www.solynaranjas.com/>
-   Mastodon (u otras redes sociales libres): <https://chaos.social/@furmans>
-   Twitter: <https://twitter.com/@dfurmans>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/dfurmans/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
