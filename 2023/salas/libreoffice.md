---
layout: 2023/post
section: proposals
category: devrooms
author: LibreOffice
title: LibreOffice
---

# LibreOffice

## Detalles de la propuesta:

>Mesa redonda para debatir sobre temas de interés en la comunidad LibreOffice

-   Formato:

>Mesa redonda

-   Público objetivo:

>Usuario de LibreOffice, desarrolladores, voluntarios

## Comunidad que propone la sala:

### LibreOffice

> Comunidad LibreOffice en España

-   Web: <https://es.libreoffice.org>
-   Twitter: <https://twitter.com/@libreoffice>

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso
