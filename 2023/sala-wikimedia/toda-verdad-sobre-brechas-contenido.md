---
layout: 2023/post
section: proposals
category: talks
author: David Abián
title: Toda la verdad sobre las brechas de contenido
---

# Toda la verdad sobre las brechas de contenido

>¿Qué son las brechas de contenido? ¿Es cierto que hay brechas por género, nivel socioeconómico, ubicación, etc. en sitios web de producción por pares como Wikipedia, Wikidata y OpenStreetMap? Se responderá con rigor científico a estas preguntas gracias a una estrategia de análisis que no te esperas.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>¿Qué son las brechas de contenido? ¿Es cierto que hay brechas por género, nivel socioeconómico, ubicación, etc. en sitios web de producción por pares como Wikipedia, Wikidata y OpenStreetMap? Se responderá con rigor científico a estas preguntas gracias a una estrategia de análisis que no te esperas.

-   Web del proyecto: <https://www.wikimedia.es/>

-   Público objetivo:

>Personas que se dedican a la investigación académica, agentes e instituciones culturales, creadores, participantes en la comunidad del software libre, wikimedistas y todas las personas que tengan interés en los temas a tratar.

## Ponente:

-   Nombre: David Abián

-   Bio:

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
