---
layout: 2023/post
section: proposals
category: talks
author: Rubén Ojeda
title: Llenando la 'España vaciada' en Wikipedia
---

# Llenando la 'España vaciada' en Wikipedia

>En esta sesión expondremos las distintas iniciativas que se están llevando a cabo para reducir la brecha de contenidos sobre el medio rural, especialmente en el ámbito de la documentación fotográfica.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>En esta sesión expondremos las distintas iniciativas que se están llevando a cabo para reducir la brecha de contenidos sobre el medio rural, especialmente en el ámbito de la documentación fotográfica.

-   Web del proyecto: <https://www.wikimedia.es/>

-   Público objetivo:

>Personas que se dedican a la investigación académica, agentes e instituciones culturales, creadores, participantes en la comunidad del software libre, wikimedistas y todas las personas que tengan interés en los temas a tratar.

## Ponente:

-   Nombre: Rubén Ojeda

-   Bio:

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
