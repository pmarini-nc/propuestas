---
layout: 2023/post
section: proposals
category: talks
author: Florencia Claes, Ester Bonet
title: La repercusión de la brecha de género de la Wikipedia en el 'Big Data'
---

# La repercusión de la brecha de género de la Wikipedia en el 'Big Data'

>La Wikipedia es uno de los grandes exponentes del procomún digital, se anuncia a sí misma como una enciclopedia basada en la neutralidad, la autoridad y el consenso. Pero ¿qué ocurre si esto no es cierto, si los datos que aporta la Wikipedia presentan brechas y sesgos de género?

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>La Wikipedia es uno de los grandes exponentes del procomún digital, se anuncia a sí misma como una enciclopedia basada en la neutralidad, la autoridad y el consenso. Pero ¿qué ocurre si esto no es cierto, si los datos que aporta la Wikipedia presentan brechas y sesgos de género?

-   Web del proyecto: <https://www.wikimedia.es/>

-   Público objetivo:

>Personas que se dedican a la investigación académica, agentes e instituciones culturales, creadores, participantes en la comunidad del software libre, wikimedistas y todas las personas que tengan interés en los temas a tratar.

## Ponente:

-   Nombre: Florencia Claes, Ester Bonet

-   Bio:

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
