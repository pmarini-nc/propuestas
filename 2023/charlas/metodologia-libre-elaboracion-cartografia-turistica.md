---
layout: 2023/post
section: proposals
category: talks
author: Maria Caudevilla Lambán
title: Metodología libre para la elaboración de cartografía turística
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-libregeo.eslib.re/2023/charlas/metodologia-libre-elaboracion-cartografia-turistica)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-libregeo.eslib.re/2023/charlas/metodologia-libre-elaboracion-cartografia-turistica";
  }
</script>
