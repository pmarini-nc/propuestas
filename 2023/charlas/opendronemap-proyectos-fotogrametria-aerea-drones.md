---
layout: 2023/post
section: proposals
category: talks
author: Joan Cano Aladid
title: El uso de OpenDroneMap en proyectos de fotogrametría aérea con drones
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-libregeo.eslib.re/2023/charlas/opendronemap-proyectos-fotogrametria-aerea-drones)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-libregeo.eslib.re/2023/charlas/opendronemap-proyectos-fotogrametria-aerea-drones";
  }
</script>
