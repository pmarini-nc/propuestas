---
layout: 2023/post
section: proposals
category: talks
author: Victor Suarez
title: Durango Computer&#58; El "Computador de casa" de los 80s en el Siglo XXI
---

# Durango Computer: El "Computador de casa" de los 80s en el Siglo XXI

>Muchos crecimos con la revolución de los Ordenadores en casa de los 80. Spectrum, Commodore, MSX,etc. Pero que ocurre si queremos crear un ordenador de los 80 hoy en día, pues el proyecto Durango te demostrará como crear un proyecto Open Hardware para crear tu propio computador con herramientas libres y tener hasta tu propio entorno de desarrollo moderno, para un ecosistema de los 80.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Spectrum, commodore, MSX, muchos los conocemos y los hemos utilizado. Pero que ocurre, si creamos un ordenador de estas características en el siglo XXI; con toda la información que tenemos hoy en día y utilizando el mítico microprocesador 6502 para tener un ordenador "en casa".<br><br>
Este es el proyecto Durango; un ordenador de los 80, en el año 2022 usando el microprocesador 6502; por lo que podremos tener la sensación de aquellos años. Además de poder mostrar las distintas herramientas que disponemos con herramientas modernas (como usar docker).

-   Web del proyecto: <https://durangoretro.github.io/web/>

-   Público objetivo:

>Esta charla esta orientada a cualquier persona que este interesada en el proyecto o en el mudillo.

## Ponente:

-   Nombre: Victor Suarez

-   Bio:

>Ingeniero por la Universidad de Almería que siempre esta montando charlas y talleres. Esta vez contando un proyecto en el que esta colaborando y que cualquiera puede participar.

### Info personal:

-   Web personal: <https://zerasul.me>
-   Twitter: <https://twitter.com/@zerasul>
-   GitLab (u otra forja) o portfolio general: <https://github.com/durangoretro>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
