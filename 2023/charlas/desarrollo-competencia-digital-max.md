---
layout: 2023/post
section: proposals
category: talks
author: Daniel Esteban Roque
title: Desarrollo de la competencia digital con MAX 11.5.1...¡sin conectarnos a Internet!
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-vitalinux.eslib.re/2023/charlas/desarrollo-competencia-digital-max)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-vitalinux.eslib.re/2023/charlas/desarrollo-competencia-digital-max";
  }
</script>
