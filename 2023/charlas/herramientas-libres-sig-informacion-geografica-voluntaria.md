---
layout: 2023/post
section: proposals
category: talks
author: Víctor Martínez
title: Herramientas libres de un SIG con Información Geografica Voluntaria
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-libregeo.eslib.re/2023/charlas/herramientas-libres-sig-informacion-geografica-voluntaria)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-libregeo.eslib.re/2023/charlas/herramientas-libres-sig-informacion-geografica-voluntaria";
  }
</script>
