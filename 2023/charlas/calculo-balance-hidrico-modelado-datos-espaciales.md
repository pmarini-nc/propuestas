---
layout: 2023/post
section: proposals
category: talks
author: Jose Lozano
title: Cálculo de balance hídrico a través del modelado de datos espaciales, en la cuenca del río Motatán, Estado Trujillo
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-libregeo.eslib.re/2023/charlas/calculo-balance-hidrico-modelado-datos-espaciales)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-libregeo.eslib.re/2023/charlas/calculo-balance-hidrico-modelado-datos-espaciales";
  }
</script>
