---
layout: 2023/post
section: proposals
category: talks
author: Héctor Banzo, Paola García
title: ¿Hachecincoqué? Contenidos interactivos en Aeducar, Moodle y WordPress
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-vitalinux.eslib.re/2023/charlas/hachecincoque-contenidos-interactivos-aeducar-moodle)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-vitalinux.eslib.re/2023/charlas/hachecincoque-contenidos-interactivos-aeducar-moodle";
  }
</script>
