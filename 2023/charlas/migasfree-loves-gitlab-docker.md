---
layout: 2023/post
section: proposals
category: talks
author: Eduardo Romero Moreno
title: Migasfree loves GitLab & Docker
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-distros-linux.eslib.re/2023/charlas/migasfree-loves-gitlab-docker)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-distros-linux.eslib.re/2023/charlas/migasfree-loves-gitlab-docker";
  }
</script>
