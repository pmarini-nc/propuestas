---
layout: 2023/post
section: proposals
category: talks
author: Ana Mª López Floría, Carlos González Larraga, Daniel Esteban, Andrea Alfaro, Sergio Salgado
title: Mesa redonda&#58; “Digitalización en las aulas y oligopolios tecnológicos”
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-vitalinux.eslib.re/2023/talleres/mesa-redonda-digitalizacion-aulas-oligopolios-tecnologicos)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-vitalinux.eslib.re/2023/talleres/mesa-redonda-digitalizacion-aulas-oligopolios-tecnologicos";
  }
</script>
