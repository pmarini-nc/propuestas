---
layout: 2023/post
section: proposals
category: talks
author: Alberto Larraz Dalmases
title: IsardVDI&#58; la solución de virtualización de escritorios que contribuye a democratizar y facilitar el acceso al software necesario para cada acción educativa
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-vitalinux.eslib.re/2023/charlas/isardvdi-virtualizacion-escritorios-accion-educativa)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-vitalinux.eslib.re/2023/charlas/isardvdi-virtualizacion-escritorios-accion-educativa";
  }
</script>
