---
layout: 2023/post
section: proposals
category: talks
author: Stella Carrera Cuadrado y Mª Isabel Pareja Moreno
title: Trabajando los ODS en el aula con un entorno digital sostenible
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-vitalinux.eslib.re/2023/charlas/trabajando-ods-aula-entorno-digital-sostenible)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-vitalinux.eslib.re/2023/charlas/trabajando-ods-aula-entorno-digital-sostenible";
  }
</script>
