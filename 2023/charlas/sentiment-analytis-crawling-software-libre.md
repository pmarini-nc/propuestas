---
layout: 2023/post
section: proposals
category: talks
author: Christian Lopez
title: Sentiment analysis & crawling con Software Libre
---

# Sentiment analysis & crawling con Software Libre

>¿Cómo hemos desarrollado un sistema de crawling y analisis de sentimiento en Twitter con software libre?<br><br>
Comentar las diferentes tecnologías y pasos que hemos seguido desde el origen del proyecto (sobre 2017) hasta el día de hoy. De salir de la facultad a diseñar e implementar todo el sistemas.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>La temática será una mezcla entre descripción de tecnologías, evolución del proyecto, problemas que hemos tenido y una visión personal de la persona que a diseñado el sistema.<br><br>
Mi intención principal sería contar mi experiencia tanto a nivel personal como tecnológico sobre la realización de un proyecto de grandes dimensiones directamente al salir de la facultad

-   Público objetivo:

>Desarrolladores, gestores de proyecto, universidades...

## Ponente:

-   Nombre: Christian Lopez

-   Bio:

>Ingeniero de software, desarrollador Drupal. Algo que destacar: el proyecto de fin de carrera ha sido un algoritmo para la deducción de la tendencia política en redes sociales.

### Info personal:

-   Twitter: <https://twitter.com/christianlrcalo>
-   GitLab (u otra forja) o portfolio general: <https://github.com/reloxo95>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
