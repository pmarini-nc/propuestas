---
layout: 2023/post
section: proposals
category: talks
author: Sara Arjona Téllez
title: Cosas chachis que pasan cuando diversos proyectos de software libre se juntan&#58; integración de H5P, BBB y Matrix con Moodle
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-vitalinux.eslib.re/2023/charlas/cosas-chachis-proyectos-software-libres-juntos)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-vitalinux.eslib.re/2023/charlas/cosas-chachis-proyectos-software-libres-juntos";
  }
</script>
