---
layout: 2023/post
section: proposals
category: talks
author: Carmen Díez SanMartín
title: Las calles de la mujeres en Aragón por Geochicas OSM
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-libregeo.eslib.re/2023/charlas/calles-mujeres-aragon-geochicasosm)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-libregeo.eslib.re/2023/charlas/calles-mujeres-aragon-geochicasosm";
  }
</script>
