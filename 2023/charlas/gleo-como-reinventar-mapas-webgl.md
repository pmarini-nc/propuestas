---
layout: 2023/post
section: proposals
category: talks
author: Iván Sánchez Ortega
title: Gleo, o cómo reinventar mapas con WebGL
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-libregeo.eslib.re/2023/charlas/gleo-como-reinventar-mapas-webgl)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-libregeo.eslib.re/2023/charlas/gleo-como-reinventar-mapas-webgl";
  }
</script>
