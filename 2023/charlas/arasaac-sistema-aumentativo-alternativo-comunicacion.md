---
layout: 2023/post
section: proposals
category: talks
author: Juan Daniel Burró Aláez
title: ARASAAC&#58; Sistema aumentativo y alternativo de comunicación
---

# ARASAAC: Sistema aumentativo y alternativo de comunicación

>ARASAAC es un Sistema Aumentativo y Alternativo de Comunicación (SAAC) basado en el uso de pictogramas que facilitan la comunicación a las personas que tienen dificultades en este ámbito por distintos factores (diversidad funcional, desconocimiento del idioma, traumatismos y degeneración cognitiva).<br><br>
ARASAAC es un proyecto del Gobierno de Aragón, con licencia Creative Commons License BY-NC-SA que nació en el año 2007. Tuvo más de 80 millones de páginas vistas durante el año 2022 y está traducido a más de 30 idiomas.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Presentaremos el proyecto ARASAAC, navegaremos por su web y conoceremos su evolución desde un sistema LAMP (2007 -2019) a nuestra arquitectura actual MERN con contenedores y vitaminas: gestión de traducciones (crowdin), monitorización (prometheus+graphana, datadog), o caché (localStorage, varnish).<br><br>
Investigaremos la API de ARASAAC descrita mediante la iniciativa OPENAPI para crear aplicaciones y veremos algún caso de uso.

-   Web del proyecto: <https://arasaac.org>

-   Público objetivo:

>Desarrolladores/devops y usuarios SAAC

## Ponente:

-   Nombre: Juan Daniel Burró Aláez

-   Bio:

>Antes profesor de informática de ciclos de FP (desarrollo web) y windsurfista en mis tiempos libres. Ahora asesor técnico, trabajando como chico de todo en ARASAAC y padre de 4 hijos que me dan y me quitan la vida.

### Info personal:

-   GitLab (u otra forja) o portfolio general: <https://github.com/juanda99>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
