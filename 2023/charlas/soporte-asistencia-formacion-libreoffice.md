---
layout: 2023/post
section: proposals
category: talks
author: Fran Ruedas, Rafael Espinar, Antonio J. Caba
title: Soporte, asistencia y formación con las aplicaciones LibreOffice
---

# Soporte, asistencia y formación con las aplicaciones LibreOffice

>Experiencias relacionadas con el soporte, asistencia y formación del paquete ofimático LibreOffice en la Administración Pública.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>En esta charla se comentará la experiencia post migración a LibreOffice en la Consejería de Hacienda de la Junta de Andalucía, comentando los problemas que nos han reportado los usuarios así como los cursos de formación que hemos realizado para el INAP (Instituto Nacional de Administración Pública).


## Ponente:

-   Nombre: Fran Ruedas, Rafael Espinar y Antonio J. Caba

-   Bio:

>Somos tres técnicos certificados de LibreOffice que atienden a los usuarios finales en una consejería de la Junta de Andalucía. En nuestro día a día tenemos que resolver, aparte de otros temas técnicos, dudas e incidencias con el paquete ofimático LibreOffice. También tutorizamos cursos Writer, Calc, Impress y Base en el Instituto Nacional de Administración Pública.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
