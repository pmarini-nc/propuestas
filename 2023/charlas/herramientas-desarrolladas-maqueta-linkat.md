---
layout: 2023/post
section: proposals
category: talks
author: Joan de Gracia, Francesc Busquets
title: Herramientas desarrolladas para la maqueta Linkat del Plan de Educación Digital de Catalunya
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-distros-linux.eslib.re/2023/charlas/herramientas-desarrolladas-maqueta-linkat)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-distros-linux.eslib.re/2023/charlas/herramientas-desarrolladas-maqueta-linkat";
  }
</script>
