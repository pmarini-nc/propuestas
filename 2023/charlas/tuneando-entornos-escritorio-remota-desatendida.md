---
layout: 2023/post
section: proposals
category: talks
author: Arturo Martín Romero (Vitalinux)
title: Tuneando entornos de escritorio de manera remota y desatendida
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-distros-linux.eslib.re/2023/charlas/tuneando-entornos-escritorio-remota-desatendida)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-distros-linux.eslib.re/2023/charlas/tuneando-entornos-escritorio-remota-desatendida";
  }
</script>
