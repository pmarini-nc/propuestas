---
layout: 2023/post
section: proposals
category: talks
author: Rafael Martínez Cebolla
title: Repositorio de código abierto de ICEARAGON
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-libregeo.eslib.re/2023/charlas/repositorio-codigo-abierto-icearagon)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-libregeo.eslib.re/2023/charlas/repositorio-codigo-abierto-icearagon";
  }
</script>
