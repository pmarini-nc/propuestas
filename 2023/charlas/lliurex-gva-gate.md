---
layout: 2023/post
section: proposals
category: talks
author: Enrique Medina Gremaldos
title: LliureX GVA Gate
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-distros-linux.eslib.re/2023/charlas/lliurex-gva-gate)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-distros-linux.eslib.re/2023/charlas/lliurex-gva-gate";
  }
</script>
