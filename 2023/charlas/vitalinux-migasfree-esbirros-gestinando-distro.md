---
layout: 2023/post
section: proposals
category: talks
author: Ignacio Sancho Morte
title: Vitalinux, migasfree y algunos esbirros... gestinando una distro
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-distros-linux.eslib.re/2023/charlas/vitalinux-migasfree-esbirros-gestinando-distro)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-distros-linux.eslib.re/2023/charlas/vitalinux-migasfree-esbirros-gestinando-distro";
  }
</script>
