---
layout: 2023/post
section: proposals
category: talks
author: Carlos Adiego Cortés
title: Introducción a Prometheus y PromQL&#58; La vida secreta de las labels
---

# Introducción a Prometheus y PromQL: La vida secreta de las labels

>Prometheus es el estándar de monitorización de aplicaciones cloud nativas. Sus dashboards y alertas son esenciales para prevenir problemas o encontrar su solución de manera rápida y eficiente.<br><br>
Uno de los problemas más frecuentes es que a veces las métricas de Prometheus no contienen toda la información que necesitamos (en sus “etiquetas”) para correlacionarlas con los sistemas que monitorizan, o que no podemos combinar diferentes métricas porque contienen etiquetas diferentes.<br><br>
Por suerte, Prometheus permite realizar operaciones de “reetiquetado” para enriquecer las métricas, y también ofrece diversas funciones para combinar diferentes métricas que resuelven estos problemas.<br><br>
En esta charla veremos ejemplos prácticos de cómo crear nuevas etiquetas con información proveniente de diversas fuentes, y explicaremos las opciones que tenemos para resolver conflictos entre métricas con etiquetas que no coinciden.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Prometheus es el estándar de monitorización de aplicaciones cloud nativas. Sus dashboards y alertas son esenciales para prevenir problemas o encontrar su solución de manera rápida y eficiente.<br><br>
Uno de los problemas más frecuentes es que a veces las métricas de Prometheus no contienen toda la información que necesitamos (en sus “etiquetas”) para correlacionarlas con los sistemas que monitorizan, o que no podemos combinar diferentes métricas porque contienen etiquetas diferentes.<br><br>
Por suerte, Prometheus permite realizar operaciones de “reetiquetado” para enriquecer las métricas, y también ofrece diversas funciones para combinar diferentes métricas que resuelven estos problemas.<br><br>
En esta charla veremos ejemplos prácticos de cómo crear nuevas etiquetas con información proveniente de diversas fuentes, y explicaremos las opciones que tenemos para resolver conflictos entre métricas con etiquetas que no coinciden.<br><br>
Cualquier usuario de Prometheus que quiera crear paneles y alertas es el público objetivo de esta charla. El reetiquetado de métricas en Prometheus y las combinaciones de métricas son herramientas muy potentes, pero muchas veces son ignoradas o incluso desconocidas por los usuarios. Y aunque conozcan de su existencia, pueden pasarlas canutas para hacer conseguir que funcionen ya que la documentación o los tutoriales son escasos, o puede que ignoren lo increíblemente útiles que son para su día a día.<br><br>
Al explicar de forma sencilla lo fácil que es usarlas, y los grandes beneficios que añaden a sus sistemas de monitorización, cambiará la percepción de que Prometheus es difícil de aprender y utilizar.

-   Web del proyecto: <https://prometheus.io/>

-   Público objetivo:

>Cualquier usuario de Prometheus que quiera crear paneles y alertas es el público objetivo de esta charla. El reetiquetado de métricas en Prometheus y las combinaciones de métricas son herramientas muy potentes, pero muchas veces son ignoradas o incluso desconocidas por los usuarios. Y aunque conozcan de su existencia, pueden pasarlas canutas para hacer conseguir que funcionen ya que la documentación o los tutoriales son escasos, o puede que ignoren lo increíblemente útiles que son para su día a día.<br><br>
Al explicar de forma sencilla lo fácil que es usarlas, y los grandes beneficios que añaden a sus sistemas de monitorización, cambiará la percepción de que Prometheus es difícil de aprender y utilizar.

## Ponente:

-   Nombre: Carlos Adiego Cortés

-   Bio:

>Carlos Adiego es “Integrations Engineer” en Sysdig, donde disfruta creando paneles de monitorización y alertas, y aprendiendo cosas cada día. También le encantan los animales, viciarse a videojuegos y leer mangas.<br><br>
También ha sido ponente en otras charlas:
- VIII DevOps Zaragoza - Service Mesh Istio
- PromCon 2022 Munich - Supercharging your metrics for easy promQL queries

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
