---
layout: 2023/post
section: proposals
category: workshops
author: Notxor
title: Concordancias entre el Software Libre y el esperanto
---

# Concordancias entre el Software Libre y el esperanto

>La Frateco de Zaragoza es la asociación esperantista más antigua de España (1908). Cuando L.L. Zamenhof, en 1887, publicó su propuesta de idioma internacional auxiliar, lo liberó para que fuera la comunidad de hablantes los que decidieran sobre su uso.<br><br>
Tanto el SL como el Eo nacieron con la idea de que la información debe ser libre, directamente usable por todas las personas que lo necesiten. Por otro lado, muchos programas y mucho SL toman su nombre del Eo, entre la comunidad de defensores del SL, hay muchos que hablamos Eo. ¿Hubiera abrazaso el SL el Dr. L.L. Zamenhof? Yo creo que sí.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>    - Charla introductoria sobre el tema del Esperanto y el Software Libre: conocimiento libre, herramientas de comunicación libres, nombres de aplicaciones de SL en Eo. Duración: 20 minutos.<br><br>
- Taller de introducción al idioma: los primeros pasos en el idioma. Duración: 45 minutos.<br><br>
Ya escribí en mi blog sobre el tema en el artículo, aunque lo hice en Esperanto: <https://notxor.nueva-actitud.org/2020/09/18/esperanto-kaj-libera-programaro.html>

-   Público objetivo:

>Cualquier persona.

## Ponente:

-   Nombre: Notxor

-   Bio:

>Psicólogo clínico. Amante del software libre, usuario de GNU/Linux desde 1998. Escribe de vez en cuando en su blog «Notxor tiene un blog» <https://notxor.nueva-actitud.org> sobre SL, especialmente sobre su editor favorito «Emacs», pero también sobre Esperanto.

### Info personal:

-   Web personal: <https://notxor.nueva-actitud.org>
-   Mastodon (u otras redes sociales libres): <https://tuiter.rocks/@Notxor>
-   GitLab (u otra forja) o portfolio general: <https://codeberg.org/Notxor>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
