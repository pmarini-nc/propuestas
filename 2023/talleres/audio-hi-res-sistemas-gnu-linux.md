---
layout: 2023/post
section: proposals
category: workshops
author: Jorge Lama Varela
title: Audio Hi-Res (o sonido de alta resolución) en sistemas GNU/Linux
---

# Audio Hi-Res (o sonido de alta resolución) en sistemas GNU/Linux

>En este taller veremos los conceptos básicos y muchos consejos para poder sacar provecho al sonido de alta resolución en sistemas GNU/Linux.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>En este taller aprenderemos los conceptos básicos para poder sacar provecho al sonido de alta resolución en sistemas GNU/Linux, consejos tanto en el uso de cierto hardware como de configuraciones de software y de ecualización de nuestra sala de escucha.También aprenderemos a usar Audacity para analizar archivos de música y a optimizar varias herramientas libres para reproducir archivos Hi-Res.<br><br>
Es recomendable traer tu portátil al taller y tener Audacity instalado en él, además de auriculares para no molestar al resto de los participantes durante las prácticas.

-   Público objetivo:

>Todas las pesonas a las que le guste escuchar música y gusten de exprimir al máximo sus equipos y aplicaciones, para conseguir la mejor calidad sonora.

## Ponente:

-   Nombre: Jorge Lama Varela

-   Bio:

>Soy miembro de varios colectivos relacionados con el mundo del software libre y la cultura libre. Me gusta la tecnología y la música, y disfruto utilizando tecnologías libres. También me gusta dar charlas y talleres, intentado dar a conocer estos mundos.<br><br>
Llevo más de 10 años produciendo podcast, por lo que tengo conocimiento en temas de grabación y edición de sonido, así como de micrófonos, interfaces de sonido, altavoces y otro tipo de cacharros (amplificadores, ecualizadores, crossovers, etc).<br><br>
Soy coordinador del Banco de reciclaje electrónico en A Coruña de Enxeñería Sen Fronteiras, donde preparamos equipos usados con software libre y los donamos a través de diferentes entidades.

### Info personal:

-   Twitter: <https://twitter.com/@raivenra>

## Comentarios

>Para el taller estaría bien disponer de un espacio con una acústica decente, una sala donde no tengamos demasiado eco, ni rebotes de sonido.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
