---
layout: 2023/post
section: proposals
category: workshops
author: Carlos Alberto Ruiz Naranjo, Ismael Pérez Rojo
title: TerosHDL&#58; un IDE para FPGA y ASIC
---

# TerosHDL: un IDE para FPGA y ASIC

>TerosHDL es un IDE open-source multiplataforma para FPGA que busca ofrecer a los desarrolladores de lenguajes de descripción de hardware muchas de las facilidades habituales de los lenguajes de programación software. TerosHDL abarca desde las funcionalidades clásicas de la mayoría de editores de código (como chequeo de sintaxis o una vista de la estructura del código) hasta la generación de documentación automatizada, test sobre múltiples simuladores, etc.<br><br>
En el taller haremos una demostración de cómo hacer un desarrollo en FPGA con herramientes libres.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>Es fácil identificar muchos entornos de desarrollo para lenguajes populares como C++, Java o Python, que además suelen ser libres o, al menos, cuentan con una versión “community”. Pero el campo de los IDEs FPGA es mucho más limitado. De hecho, TerosHDL surgió a partir de nuestras necesidades. Necesitábamos un entorno de trabajo que fuera personalizable, que se adaptara fácilmente al workflow del equipo de trabajo en vez de lo contrario y, además, que se pudiera integrar con muchas de las herramientas que la comunidad está desarrollando actualmente.

-   Web del proyecto: <https://terostechnology.github.io/terosHDLdoc/>

-   Público objetivo:

>Va dirigida tanto a desarrolladores de HDL (FPGA y ASIC) como a los que se están iniciando en el hardware libre.


## Ponente:

-   Nombre: Carlos Alberto Ruiz Naranjo, Ismael Pérez Rojo

-   Bio:

>- Carlos Alberto Ruiz: Ingeniero Senior de FPGA en Software Radio Systems Ltd.<br><br>
- Ismael Pérez: Ingeniero Senior de FPGA en Quside.

### Info personal:

-   Web personal: <https://www.linkedin.com/in/carlos-alberto-ruiz-fpga/> / <https://www.linkedin.com/in/ispero/>
-   GitLab (u otra forja) o portfolio general: <https://github.com/qarlosalberto>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
