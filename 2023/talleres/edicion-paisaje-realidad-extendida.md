---
layout: 2023/post
section: proposals
category: workshops
author: Antonio Montes (inventadero)
title: Edición del Paisaje en realidad extendida (XR)
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-libregeo.eslib.re/2023/talleres/edicion-paisaje-realidad-extendida)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-libregeo.eslib.re/2023/talleres/edicion-paisaje-realidad-extendida";
  }
</script>
