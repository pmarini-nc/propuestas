---
layout: 2023/post
section: proposals
category: workshops
author: Alejandro López
title: Reparando mi propio ordenador portátil con Slimbook
---

# Reparando mi propio ordenador portátil con Slimbook

>En este taller de 2 horas, aprenderemos a abrir y desmontar un ordenador portátil. Usaremos algunas unidades de SLIMBOOK, pero la teoría es muy similar en otras marcas, por lo que, si lo deseas, traete tu ordenador y te ayudaremos. El mantenimiento de un ordenador portátil, pasa pro una limpieza correcta, y es algo tan importante, como las revisiones de un vehículo.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>Además, llevaremos equipamiento, como destornilladores, paletillas, pinzas, y pasta térmica, para el que se atreva a cambiarla!

-   Web del proyecto: <https://www.slimbook.es>

## Ponente:

-   Nombre: Alejandro López

-   Bio:

>Fundador en 2015 de Slimbook, aunque usuario de Debian desde 2003, Alejandro es un programador que se adentró en los caminos del pingûino, y apostó por proveer de hardware compatible y de alta calidad, a nuestras queridas distribuciones, olvidadas por las grandes marcas.

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://linuxrocks.online/@slimbook>
-   Twitter: <https://twitter.com/@slimbook>
-   GitLab (u otra forja) o portfolio general: <https://github.com/Slimbook-Team>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
