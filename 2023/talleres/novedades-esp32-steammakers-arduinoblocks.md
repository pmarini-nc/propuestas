---
layout: 2023/post
section: proposals
category: workshops
author: Toni Moreno
title: Novedades ESP32 STEAMakers y ArduinoBlocks para el nuevo currículum
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-vitalinux.eslib.re/2023/talleres/novedades-esp32-steammakers-arduinoblocks)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-vitalinux.eslib.re/2023/talleres/novedades-esp32-steammakers-arduinoblocks";
  }
</script>
