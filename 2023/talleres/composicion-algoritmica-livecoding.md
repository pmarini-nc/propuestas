---
layout: 2023/post
section: proposals
category: workshops
author: Alejandro Rodríguez Antolín, Miguel Ángel Rodríguez Muiños
title: Composición algorítmica y “Live Coding”
---

# Composición algorítmica y “Live Coding”

>La composición algorítmica consiste en crear música utilizando un lenguaje de programación. Si la composición se realiza en sesiones "en directo" recibe el nombre de livecoding. En este taller se aprenderan los conocimientos básicos sobre composición algoritmica y livecoding usando el software libre Sonic-Pi (<https://sonic-pi.net/>) que nos permitirá crear (programando) nuestras propias canciones, melodías, lineas de instrumentos (batería, bajo, ..) y/o sesiones de DJ en directo.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>En este taller, de 2 horas de duración, utilizaremos Sonic-Pi (software libre multiplataforma -GNU/Linux, macOS, Windows-) y aprenderemos a instalarlo (es recomendable traer nuestro portátil) y a utilizarlo para realizar nuestras propias creaciones (es recomendable traer auriculares para no molestar a los demás). El objetivo de este taller es el de aprender los fundamentos de programación en Sonic-Pi y el manejo de su interfaz gráfica para poder componer, interpretar y/o grabar nuestras propias creaciones.

-   Público objetivo:

>Desarrolladores, músicos e interesados en aprender a crear música programando. Formadores de introduccion a la programación que quieran alternativas a la robótica.

## Ponente:

-   Nombre: Alejandro Rodríguez Antolín y Miguel Ángel Rodríguez Muiños

-   Bio:

>Alejandro Rodríguez Antolín: Musicólogo por la Universidad Autónoma de Madrid. Doctor en ciernes (especialidad de Música electroacústica en España). Friki de la música y de los videojuegos y aficionado al ajedrez.<br><br>
Miguel Ángel Rodríguez Muíños: Técnico Informático de profesión. Caballero andante del Software Libre por vocación y músico amateur por afición. Miembro de la Asociación de Usuarios de Software Libre MELISA.

### Info personal:

-   Twitter: <https://twitter.com/@mianromu>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
