---
layout: 2023/post
section: proposals
category: workshops
author: Miguel Sevilla-Callejo
title: Taller de introducción a la edición en OpenStreetMap
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-libregeo.eslib.re/2023/talleres/taller-introduccion-edicion-openstreetmap)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-libregeo.eslib.re/2023/talleres/taller-introduccion-edicion-openstreetmap";
  }
</script>
