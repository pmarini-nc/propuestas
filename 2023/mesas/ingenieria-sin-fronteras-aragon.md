---
layout: 2023/post
section: proposals
category: tables
author: Ingeniería Sin Fronteras Aragón
title: Ingeniería Sin Fronteras Aragón
---

## Ingeniería Sin Fronteras Aragón

-   Web: <https://aragon.isf.es/>

>Somos una asociación basada en el voluntariado, asamblearia e interdisciplinar con sede en Zaragoza. Queremos construir una sociedad justa y sostenible poniendo la tecnología apropiada al servicio del desarrollo humano. Para ello, llevamos a cabo actividades de formación, proyectos de cooperación e incidencia política. Tenemos diferentes líneas de trabajo como electrónica ética, derecho al agua, soberanía alimentaria, Sáhara o energía. Siempre con un enfoque de género y buscando trabajar en red con otras asociaciones afines.

-   Twitter: <https://twitter.com/isfAragon>

## Comentarios

>Llevaremos información sobre los distintos temas y actividades que tratamos y algunos materiales que puedan resultar interesantes. Además, intentaremos que haya alguien en la mesa la mayor parte del tiempo pero eso dependerá un poco de la disponibilidad de las personas voluntarias.

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso
