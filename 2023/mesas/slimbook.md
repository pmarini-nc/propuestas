---
layout: 2023/post
section: proposals
category: tables
author: Slimbook
title: Slimbook
---

## Slimbook

-   Web: <https://slimbook.es>

>Slimbook es una empresa valenciana, que desde 2015 intenta ofrecer hardware de alta calidad, con sistemas operativos GNU/Linux. En todos los eventos a los que vamos, los asistentes disfrutan de poder tocar y probar nuestros juguetes :)

-   Mastodon (u otras redes sociales libres): <https://linuxrocks.online/@slimbook>
-   Twitter: <https://twitter.com/slimbook>
-   GitLab (u otra forja) o portfolio general: <https://github.com/Slimbook-Team>

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso
