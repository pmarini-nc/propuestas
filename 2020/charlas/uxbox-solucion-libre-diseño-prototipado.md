---
layout: 2020/post
section: proposals
category: talks
title: UXBOX, la solución libre para diseño y prototipado
---

El desarrollo de UXBOX está en un punto dulce y queremos compartirlo con vosotros. Daros a conocer la herramienta, sus funcionalidades principales y los objetivos a cumplir.

-   Más info en <https://uxbox.io>
-   Proyecto en GitHub [https://github.com/uxbox](https://uxbox.io)

## Formato de la propuesta

Indicar uno de estos:

-   [x]  Charla (25 minutos)
-   [ ]  Charla relámpago (10 minutos)

## Descripción

UXBOX, la solución libre para diseño y prototipado, hacia su versión alpha. UXBOX es una nueva plataforma web libre basada en estándares abiertos SVG y enfocada a equipos multidisciplinares que construyen productos digitales. En la charla haremos una breve introducción de nuestros primeros 10 meses de vida para contar qué enfoque y qué funcionalidades estamos desarrollando para nuestra Alfa a finales de año. También hablaremos de cómo estamos planteando la creación de una comunidad sana e inclusiva alrededor del proyecto y cual es el plan de proyecto previsto.

El panorama del diseño Open Source ha ido mejorando con el paso del tiempo pero aún está en sus inicios. Con UXBOX queremos impulsar todo este movimiento y crear una herramienta que abarque todos los roles dentro de un equipo. Desde diseño hasta marketing pasando todas las etapas de desarrollo.

## Público objetivo

Todos los públicos.

## Ponente(s)

-   **Sara Gil Casanova**: UXBOX comms. Especialista en comunicación y enfocada en la comunidad Open Source.
-   **Juan de la Cruz**: Diseñador y co-fundador de UXBOX. Enfocado en el diseño de interfaz y de interacción. Seguidor de la comunidad Open Source Design.

### Contacto(s)

-   **Juan de la Cruz**: juan.delacruz at kaleidos dot net

## Comentarios

Trataremos de hacer una demo si nos da tiempo :)

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x]  Al menos una persona entre los que la proponen estará presente el día programado para la charla.
