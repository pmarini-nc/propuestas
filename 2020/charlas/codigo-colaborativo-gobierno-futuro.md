---
layout: 2020/post
section: proposals
category: talks
title: Código colaborativo&#58 el gobierno del futuro
---

En el siglo XXI el software puede ser considerado como una infraestructura pública vital. Desde servicios de parking municipales hasta aquellos de administración de escuelas públicas, el software se ha convertido en la piedra angular del funcionamiento de las organizaciones públicas.

A pesar de ello, el software que impera en nuestra sociedad actual se encuentra con frecuencia escondido en projectos de código ocultos, produciendo datos a los que la ciudadanía no tiene acceso y licencias cuyo coste no deja de incrementarse.

¿Podemos hacer esta infraestructura pública funcionar sin cajas negras de código?

## Formato de la propuesta

Indicar uno de estos:

-   [x]  Charla (25 minutos)
-   [ ]  Charla relámpago (10 minutos)

## Descripción

¿Cómo podemos hacer a los ciudadanos más partícipes de las decisiones políticas? ¿Cómo hacer más asequible la participación en un proyecto de código por parte de una localidad pequeña? Hace más de un año que en Foundation for Public Code trabajamos con administraciones públicas locales, provinciales, nacionales y supranacionales ayudándoles a que sus proyectos se conviertan en public code, donde el open source no solo afecta al código, sino también a la legislación y procesos circundantes al proyecto.

En esta charla compartiré algunas de las actividades y herramientas que desarrollamos junto a dichos organismos para contribuir a un ecosistema más abierto y por lo tanto democrático.

## Público objetivo

Esta presentación va dirigida a cualquier persona interesada en las tecnologías abiertas y open source. No entraré en ningún momento a analizar código.

## Ponente(s)

Alba Roza trabaja como Codebase Steward for Communities en Foundation for Public Code en Ámsterdam. Su experiencia profesional se centra en el ámbito de Developer Relations, en el que lleva trabajando tanto en Comunicación como en Marketing orientados a profesionales tecnológicos desde 2011. Es una cara conocida en conferencias técnicas y especializada en comunidades de software, el foco de su carrera profesional durante los últimos seis años.

Antes de unirse a Foundation for Public Code trabajó para Codemotion, una de las mayores series de conferencias europeas como Project Manager de Communities del Norte de Europa. Aquí trabajó directamente con comunidades de software, principalmente españolas, holandesas y alemanas.

Para más información síguela en [Twitter](https://twitter.com/Alba_Roza).

### Contacto(s)

-   Alba Roza Suárez: alba at publiccode dot net

## Comentarios

También podría dar la charla en inglés.

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x]  Al menos una persona entre los que la proponen estará presente el día programado para la charla.
