---
layout: 2020/post
section: proposals
category: talks
title: ¿Está (realmente) roto el open source?
---

En los últimos meses está habiendo mucho debate en torno a [la definición del open source](https://opensource.org/osd) y el [free software](https://es.wikipedia.org/wiki/Software_libre), y me gustaría aportar mi humilde opinión sobre estos temas para abrir un debate entre las personas asistentes a la charla.

## Formato de la propuesta

-   [x]  Charla (25 minutos)
-   [ ]  Charla relámpago (10 minutos)

## Descripción

Se dice que el open source ha ganado, que es el estándar para desarrollo de software, e incluso corporaciones que antes lo despreciaban, hoy defienden sus bondades y exhiben orgullosas sus contribuciones.

Otras voces no opinan igual. Dicen que el open source sólo es un mecanismo para que grandes proveedores se aprovechen del esfuerzo de otras personas en beneficio propio, sin contribuir de vuelta.

Han surgido iniciativas para dotar de recursos a las personas que mantienen proyectos open source, y aún así, hay personas quemadas, empresas e iniciativas para cambiar las licencias e impedir la explotación de sus proyectos bajo ciertas condiciones.

¿Está roto el modelo? Quizás el uso sin entender las implicaciones, sin pensar en contribuir a la sostenibilidad de los proyectos, o incluso esperando recibir servicios de forma gratuita, estén ayudando a romperlo.

Está claro que el ecosistema no es el mismo que hace 20 ni 30 años. Durante esta charla me gustaría compartir con la audiencia mis reflexiones sobre estos cambios, los riesgos y oportunidades alrededor del término open source. Todo ello esperando generar visión crítica y debate sobre la evolución futura del ecosistema.

## Público objetivo

Cualquier persona interesada en usar, aprender, o contribuir a proyectos open source, tanto propios como de terceros.

## Ponente(s)

**Manrique** es CEO y socio en [Bitergia](https://bitergia.com), y un apasionado del software libre y las comunidades de desarrollo. Es ingeniero industrial, con experiencia en investigación y desarrollo en el [Centro Tecnológico de Informática y Comunicaciones del Principado de Asturias (CTIC)](https://www.fundacionctic.org/), grupos de trabajo del [W3C](https://w3.org), Ándago Engineering y Continua Health Alliance. Ex-director ejecutivo de la [Asociación Española de Empresas de Código Abierto (ASOLIF)](https://asolif.es) y ex-consultor experto del Centro Nacional de Referencia de Código Abierto (CENATIC).

Involucrado en varias comunidades relacionadas con el software libre, libre y de código abierto, está actualmente activo en los proyectos [GrimoireLab](https://chaoss.github.io/grimoirelab) y [CHAOSS (Community Health Analytics for Open Source Software)](https://chaoss.community). Ha sido elegido como AWS Data Hero y GitLab Community Hero.

Puedes contactarle por Twitter como [@jsmanrique](https://twitter.com/jsmanrique), y cuando no está en línea, le encanta pasar tiempo con su familia y hacer surf.

### Contacto(s)

-   **José Manrique López de la Fuente**: jsmanrique at gmail com | @jsmanrique

## Comentarios

Ninguno en particular.

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x]  Al menos una persona entre los que la proponen estará presente el día programado para la charla.
